import json
import requests


def main():
    prodotto = input("Inserisci il nome del prodotto che si vuole cercare: ")
    print(ricera_prodotto(prodotto))


def ricera_prodotto(prodotto):
    params = {
        'api_key': '45582197CA76485AA134AB31792E5128',
        'type': 'search',
        'amazon_domain': 'amazon.it',
        'search_term': prodotto
    }
    api_result = requests.get('https://api.rainforestapi.com/request', params)
    file = open("static/product.json", "w")
    file.write(json.dumps(api_result.json()))
    file.close()
    data = json.load(open("static/product.json"))
    # nome, prezzo
    return data['search_results'][0]['title'], data['search_results'][0]['price']['value'], data['search_results'][0]['image']


if __name__ == '__main__':
    main()
