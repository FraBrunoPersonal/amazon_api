from flask import Flask, render_template, redirect, url_for, request
import json
import requests

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():

    if request.method == 'POST':
        prodotto = request.form['prodotto']
        tipoProdotto = request.form['tipoProdotto']
        nome, prezzo, url = ricera_prodotto(prodotto)
        if tipoProdotto == "CPU":
            return render_template('index.html', nome_cpu=nome, prezzo_cpu=prezzo, url_cpu=url)
        if tipoProdotto == "GPU":
            return render_template('index.html', nome_gpu=nome, prezzo_gpu=prezzo, url_gpu=url)

    return render_template('index.html')


def ricera_prodotto(prodotto):
    params = {
        'api_key': '45582197CA76485AA134AB31792E5128',
        'type': 'search',
        'amazon_domain': 'amazon.it',
        'search_term': prodotto
    }
    api_result = requests.get('https://api.rainforestapi.com/request', params)
    file = open("product.json", "w")
    file.write(json.dumps(api_result.json()))
    file.close()
    data = json.load(open("product.json"))
    # nome, prezzo
    return data['search_results'][0]['title'], data['search_results'][0]['price']['value'], data['search_results'][0][
        'image']


if __name__ == '__main__':
    app.run(host='127.0.0.1', debug='on')
