const socket = io();

function test() {
    // client-side
    socket.emit("nome_prodotto", "ryzen 5 3600");
    // server-side
    socket.on("risultato_ricerca", (arg) => {
        console.log(arg); // world
    });
}