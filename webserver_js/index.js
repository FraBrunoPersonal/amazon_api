const app = require('express')();
const http = require('http').createServer(app);
const axios = require('axios');
const express = require("express");
const options = { /* ... */ };
const io = require('socket.io')(http, options);

app.use(express.static('public'));
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

http.listen(3000, () => {
    console.log('listening on *:3000');
});

function search_product(name_product) {
    const params = {
        api_key: "45582197CA76485AA134AB31792E5128",
        type: "search",
        amazon_domain: "amazon.it",
        search_term: name_product
    }
    axios.get('https://api.rainforestapi.com/request', { params })
        .then(response => {
            let result = JSON.stringify(response.data, 0, 2);
            result = JSON.parse(result)
            //console.log(result['search_results'][0]['title'] + "\n" + result['search_results'][0]['price']['value'] + "\n" + result['search_results'][0]['image']);
            return (result['search_results'][0]['title'] + "_" + result['search_results'][0]['price']['value'] + "_" + result['search_results'][0]['image']);

        }).catch(error => {
        console.log(error);
    })
}

//var result;
//var arg_global;
// server-side
io.on('connection', (socket) => {
    socket.on("nome_prodotto", (arg) => {
        socket.emit("risultato_ricerca", search_product(arg));
        //arg_global = arg;
    });
});
/*
console.log(arg_global); // world
result = search_product(arg_global)
io.on("connection", (socket) => {
    socket.emit("risultato_ricerca", result);
});*/
